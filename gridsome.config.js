// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const postcss_import = require('postcss-import');
const tailwindcss = require('tailwindcss');
const postcss_preset_env = require('postcss-preset-env');
const postcss_purgecss = require('@fullhuman/postcss-purgecss');

module.exports = {
  siteName: 'Escola Arcàdia',
  siteUrl: 'https://arcadiacoop.netlify.com',
  siteDescription:
    'Escola Arcàdia, centre educatiu autogestionari de Can Batlló',

  configureWebpack: {
    node: {
      fs: 'empty',
    },
  },

  chainWebpack(config) {
    config.module.rules.delete('svg');
    config.module
      .rule('svg')
      .test(/\.svg$/)
      .use('vue')
      .loader('vue-loader')
      .end()
      .use('svg-to-vue-component')
      .loader('svg-to-vue-component/loader');
  },

  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          postcss_import,
          tailwindcss,
          postcss_preset_env({ stage: 1 }),
          ...(process.env.NODE_ENV === 'production' ? [postcss_purgecss] : []),
        ],
      },
    },
  },

  transformers: {
    remark: {
      slug: false,
    },
    yamlPlus: {
      images: {
        fieldName: 'image',
        mediaPath: '../../static',
      },
      markdown: {
        fieldName: 'text',
      },
    },
  },

  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/home/*.yml',
        typeName: 'HomePageContent',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/escola/cooperativa/*.yml',
        typeName: 'CooperativaPageContent',
        yamlPlus: {
          images: {
            mediaPath: '../../../static',
          },
        },
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/escola/objectius/*.yml',
        typeName: 'ObjectiusPageContent',
        yamlPlus: {
          images: {
            mediaPath: '../../../static',
          },
        },
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/escola/funcionament/*.yml',
        typeName: 'FuncionamentPageContent',
        yamlPlus: {
          images: {
            mediaPath: '../../../static',
          },
        },
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/principis/*.yml',
        typeName: 'PrincipisPageContent',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/projecte/*.yml',
        typeName: 'ProjectePageContent',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/contacte/*.yml',
        typeName: 'ContactePageContent',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/legal/*.md',
        typeName: 'LegalPageContent',
      },
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/404/*.yml',
        typeName: 'NotFoundPageContent',
      },
    },
    {
      use: '@gridsome/plugin-sitemap',
    },
    {
      use: 'gridsome-plugin-netlify-cms',
    },
  ],
};

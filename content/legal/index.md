---
meta:
  title: Legal
  description: Escola cooperativa Arcàdia, centre educatiu autogestionari de Can Batlló...

cover:
  headline: Avís legal
  cover_image:
    image: /media/home-cover.jpg
    description: Imatge de portada de Can Batlló
---

#### Titularitat

El domini www.XXX.XX és titularitat de la XXX, amb el NIF X-XXX. L’organització consta inscrita al Registre de XXX del
Departament XXX de la Generalitat de Catalunya amb el número XXX de la secció X. Les nostres dades de contacte són:
carrer XXX, XX de Barcelona (000000). Tf: XXX. Email: XXX@XXX.XX.

#### Responsabilitat dels continguts i ús

La persona usuària adopta el compromís de fer un ús correcte de tots els serveis i continguts del lloc web, obligant-se a no danyar, canviar,
ni modificar, en cap cas, el codi, les dades o els documents del lloc web, ni tampoc introduir en el nostre sistema cap mena de programari maliciós
o dispositius que puguin alterar el servidor del lloc web ni els mitjans de protecció i seguretat. L’organització ha adoptat les mesures de seguretat
adients, segons l’estat actual de la tècnica, per tal de garantir el correcte funcionament del lloc web i evitar la generació de danys a les persones
usuàries. L’organització no es fa responsable de les pèrdues o perjudicis que es puguin generar a la persona usuària per virus informàtics,
interferències, interrupcions, omissions, avaries, desconnexions, indisponibilitat del sistema o qualsevol altre comportament anòmal del lloc web.
L’organització podrà en qualsevol moment i sense previ avís, suspendre temporalment l’accés al lloc web per efectuar les modificacions i millores que
estimi convenient i procedir al seu manteniment.

L’organització no assumirà cap tipus de responsabilitat derivada o que se’n pugui derivar de la utilització per part de les persones usuàries dels
continguts i informacions que constin al lloc web.

#### Propietat intel·lectual i llicència

Aquest lloc web i els seus continguts són d'exclusiva propietat de l’organització o s'ofereixen sota les convenients autoritzacions dels seus autors
titulars, com a conseqüència dels acords subscrits amb tercers, i estan sotmesos a drets de propietat intel·lectual i industrial i protegits per la
legislació nacional i internacional.

Els nostres continguts s’ofereixen sota la protecció d'una llicència _[Creative Commons](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.ca)_ amb les següents atribucions:

- Reconeixement: en qualsevol explotació de l’obra autoritzada per la llicència caldrà reconèixer l’autoria.
- No comercial: l’explotació de l’obra queda limitada a usos no comercials.
- Compartir igual: l’explotació autoritzada inclou la creació d’obres derivades sempre que mantinguin la mateixa llicència en ser divulgades.

En aplicació de la Llei 19/2014, del 29 de desembre, de transparència, accés a la informació pública i bon govern, la reutilització dels continguts
sobre l'organització és lliure i no està subjecte a restriccions, sempre i quan no s’alteri el contingut de la informació reutilitzada ni es
desnaturalitzi el sentit, i es citi la font de les dades.

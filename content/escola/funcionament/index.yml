meta:
  title: Funcionament
  description: L’escola cooperativa Arcàdia és un projecte pedagògic triplement autogestionat en la gestió, en l'organització i en l'aprenentatge...

cover:
  headline: Com funcionem
  cover_image:
    image: /media/funcionament-cover.jpg
    description: Imatge de portada de Can Batlló

selfManagement:
  heading: Un projecte pedagògic triplement autogestionat
  text: >-
    L’escola Arcàdia és un projecte autogestionari, cooperatiu i comunitari
    d’educació en i per la llibertat responsable, que comprèn les etapes
    d’educació infantil, primària i secundària obligatòria i què assumeix
    els principis d’autogestió de Can Batlló a tres nivells
  corePoints:
    - heading: Autogestió en la gestió
      text: >-
        La gestió del centre està dissenyada seguint els criteris autogestionaris
        i la legalitat vigent, mitjançant una cooperativa sense ànim de lucre.
        Aquesta cooperativa es constitueix amb mecanismes de radicalitat laboral,
        com la supressió d'escales salarials, l'assumpció de l'assemblea com a únic òrgan decisori
        i una política interna d'economia feminista. I tot això coordinat orgànicament
        amb l'Impuls cooperatiu de Sants i l'assemblea General de Can Batlló
        a la que rendeix comptes anualment.
      icon: /media/treball-en-equip-icon.svg

    - heading: Autogestió en l'organització
      text: >-
        A l'Arcàdia, l'organització del centre està en mans dels nens i nenes,
        l'equip educatiu i l'entorn familiar i comunitari. Organitzada l'escola,
        per diferents assemblees, comissions, cooperatives escolars i grups naturals,
        tots ells vinculats entre si, de manera orgànica i assembleària,
        tenen capacitat per decidir sobre tots aquells aspectes que els incumbeixen el seu quotidià.
        L'objectiu és desenvolupar un entorn escolar versàtil des d'on emergeixin
        els significats compartits d'una cultura escolar dialògica.
      icon: /media/estrategia-icon.svg

    - heading: Autogestió en l'aprenentatge
      text: >-
        Adquirir destreses pel control del propi procés d'aprenentatge és la fita que s'exerceix
        des de ben petits i petites, sempre respectant els diferents ritmes i el moment maduratiu
        de cada persona. Això ho fem progressant en una autoimatge ajustada, gaudint en tot moment del procés
        i acceptant l'error com a part constituent del mateix. Així, poc a poc, el nen i la nena van aprenent
        a retardar la gratificació i a desenvolupar un fort sentiment de plaer per l'aprenentatge autònom,
        el coneixement i el pensament crític.
      icon: /media/abac-icon.svg

communityTransformation:
  items:
    - title: Un projecte en relació amb l'entorn i la comunitat
      text: >-
        L'arcàdia és un centre comunitari on tota la comunitat hi té la opció de participar,
        tant els nens i les nenes, les seves tutores, la comunitat de Can Batlló,
        veïns i veïnes i persones interessades en un espai versàtil on a dur-hi usos comunitaris
        més enllà dels escolars.


        Tant mateix l'escola es vincula en la trama associativa del barri i fa ús compartit d'espais extramurs,
        com horts, tallers de Can Batlló, biblioteques o altres centres culturals i educatius creant, així,
        un espai porós on s'hi irradii coneixement mutu, confiança, complicitats i acceptació.
      item_image:
        image: /media/funcionament-entorn.jpg
        description: Nen i àvia asseguts a l'exterior enmig de la natura

    - title: Un projecte social amb vocació pública
      text: >-
        L’escola es constitueix com un projecte social público-comunitari, buscant en tot moment
        oferir una educació a l’abast de totes les classes socials, procedència o diversitat funcional.


        L’Arcàdia però, és un projecte de classe què fomenta una coeducació integral on es revaloritza
        el treball manual com a font de cultura obrera i es generen mecanismes de compensació econòmica
        per a abolir impediments econòmics.


        En quant a criteris d’accessibilitat s’hi apliquen mesures de discriminació positiva
        per tal d’assolir un mapa demogràfic aproximat al del barri.
      item_image:
        image: /media/funcionament-vocacio-publica.jpg
        description: Exterior d'un mas vist a través de la porta d'entrada

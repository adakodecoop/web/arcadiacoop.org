'use strict';

const baseURL = '/.netlify/functions';
const headers = {
  'Content-Type': 'application/json',
};

export const api = {
  subscribe: async email => {
    const response = await fetch(`${baseURL}/subscribe`, {
      headers,
      method: 'POST',
      body: JSON.stringify({ email }),
    });

    if (!response.ok) {
      console.log(`Error (${response.status}): ${response.statusText}`);
      throw new Error('Network response was not ok.');
    }

    return await response.json();
  },
};

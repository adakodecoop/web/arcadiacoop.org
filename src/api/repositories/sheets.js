'use strict';

import { google } from 'googleapis';

const {
  GOOGLE_AUTH_CREDENTIALS,
  GOOGLE_AUTH_TOKENS,
  GOOGLE_SPREADSHEET_ID,
  GOOGLE_SHEET_NAME,
} = process.env;

export const api = {
  authorize: async () => {
    const credentials = JSON.parse(GOOGLE_AUTH_CREDENTIALS);
    const tokens = JSON.parse(GOOGLE_AUTH_TOKENS);

    const { client_id, client_secret, redirect_uris } = credentials.installed;
    const client = new google.auth.OAuth2(
      client_id,
      client_secret,
      redirect_uris[0]
    );
    client.setCredentials(tokens);

    return google.sheets({
      version: 'v4',
      auth: client,
    });
  },

  append: async (sheets, email) => {
    const { data } = await sheets.spreadsheets.values.append({
      spreadsheetId: GOOGLE_SPREADSHEET_ID,
      range: `${GOOGLE_SHEET_NAME}!A1`,
      valueInputOption: 'USER_ENTERED',
      resource: { values: [[email]] },
    });

    return data;
  },
};

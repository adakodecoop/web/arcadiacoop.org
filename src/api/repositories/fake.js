'use strict';

export const api = {
  subscribe: async email => {
    return await api.fakeit(email);
  },
  fakeit: async _ => {
    console.log('fakeit', _);

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (Math.random() >= 0.5) {
          resolve();
        } else {
          reject(new Error('Network response was not ok.'));
        }
      }, 1500);
    });
  },
};

'use strict';

import { httpErrors } from '../../utils/enums';
import { api as gsheets } from '../repositories/sheets';

/**
 *
 * @param {object} error
 */
const buildErrorResponse = error => {
  console.log(error); // output to netlify function log

  if (error.message === httpErrors.METHOD_NOT_ALLOWED) {
    return { statusCode: 405, body: 'Method Not Allowed' };
  }

  return {
    statusCode: 500,
    body: 'Internal Server Error',
  };
};

/**
 *
 * @param {object} event
 */
const validateRequest = ({ httpMethod }) => {
  if (httpMethod !== 'POST') {
    throw new Error(httpErrors.METHOD_NOT_ALLOWED);
  }

  return true;
};

/**
 *
 * @param {object} event
 */
const manageSubscription = async ({ body }) => {
  const { email } = JSON.parse(body);
  return await addToSubscriptionsSheet(email);
};

/**
 *
 * @param {string} email
 */
const addToSubscriptionsSheet = async email => {
  const sheets = await gsheets.authorize();
  const data = await gsheets.append(sheets, email);

  return { data };
};

/**
 *
 * @example
 * netlify functions:invoke subscribe --no-identity --payload '{"email": "jane.doe@ac.me"}'
 *
 * @param {object} event
 * @param {object} context
 */
exports.handler = async (event, context) => {
  try {
    validateRequest(event);

    const response = await manageSubscription(event);

    return {
      statusCode: 200,
      body: JSON.stringify(response),
    };
  } catch (error) {
    return buildErrorResponse(error);
  }
};

// @ts-nocheck
import PrincipisRenovacioPedagogica from './PrincipisRenovacioPedagogica.vue';
import PrincipisPilarsTeorics from './PrincipisPilarsTeorics.vue';

export default { PrincipisRenovacioPedagogica, PrincipisPilarsTeorics };

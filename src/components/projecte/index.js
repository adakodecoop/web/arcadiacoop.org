// @ts-nocheck
import ProjecteTecnicIntro from './ProjecteTecnicIntro.vue';
import ProjecteTecnicEspai from './ProjecteTecnicEspai.vue';
import ProjecteTecnicDestacats from './ProjecteTecnicDestacats.vue';
import ProjecteTecnicEnDades from './ProjecteTecnicEnDades.vue';

export default {
  ProjecteTecnicIntro,
  ProjecteTecnicEspai,
  ProjecteTecnicDestacats,
  ProjecteTecnicEnDades,
};

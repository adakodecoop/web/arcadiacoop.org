// @ts-nocheck
import HomeConceptsSummary from './HomeConceptsSummary.vue';
import HomeConceptsInDetail from './HomeConceptsInDetail.vue';

export default { HomeConceptsSummary, HomeConceptsInDetail };

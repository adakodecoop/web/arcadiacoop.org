// @ts-nocheck
import ObjectiusAmbitsTreball from './ObjectiusAmbitsTreball.vue';
import ObjectiusExperienciaEscolar from './ObjectiusExperienciaEscolar.vue';

export default { ObjectiusAmbitsTreball, ObjectiusExperienciaEscolar };

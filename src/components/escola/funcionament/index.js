// @ts-nocheck
import FuncionamentAutogestio from './FuncionamentAutogestio.vue';
import FuncionamentTransformacioComunitaria from './FuncionamentTransformacioComunitaria.vue';

export default { FuncionamentAutogestio, FuncionamentTransformacioComunitaria };

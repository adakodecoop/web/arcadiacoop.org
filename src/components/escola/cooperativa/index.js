// @ts-nocheck
import CooperativaQuiSom from './CooperativaQuiSom.vue';
import CooperativaQueFem from './CooperativaQueFem.vue';
import CooperativaOnSom from './CooperativaOnSom.vue';

export default { CooperativaQuiSom, CooperativaQueFem, CooperativaOnSom };

// @ts-nocheck

// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

// Import Fonts
import 'typeface-montserrat';
import 'typeface-source-sans-pro';

// Import Global Styles
import '~/assets/styles/index.css';

// Import Global Dependencies
import LazyHydrate from 'vue-lazy-hydration';
import VueScrollTo from 'vue-scrollto';

// Import Global Components
import HyperLink from '~/components/common/HyperLink';
import ButtonLink from '~/components/common/ButtonLink';

// Import Layouts
import PageLayout from '~/layouts/Page';

export default function(Vue, { router, head, isClient }) {
  // Third-party Global Components
  Vue.component('LazyHydrate', LazyHydrate);

  // Base global components
  Vue.component('HyperLink', HyperLink);
  Vue.component('ButtonLink', ButtonLink);

  // Set default layout as a global component
  Vue.component('PageLayout', PageLayout);

  // Use Vue Plugins
  Vue.use(VueScrollTo);

  // Define head metadata
  head.htmlAttrs = {
    lang: 'ca',
  };
}

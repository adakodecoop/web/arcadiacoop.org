module.exports = {
  theme: {
    colors: {
      transparent: 'transparent',

      black: '#000',
      white: '#fff',

      gray: {
        100: '#f7fafc',
        200: '#edf2f7',
        300: '#e2e8f0',
        400: '#cbd5e0',
        500: '#a0aec0',
        600: '#718096',
        700: '#4a5568',
        800: '#2d3748',
        900: '#1a202c',
      },
      red: {
        100: '#F0DCDD',
        200: '#F5A3A5',
        300: '#F66C6F',
        400: '#F14146',
        500: '#E4222B', //base//
        600: '#B41D2D',
        700: '#80192A',
      },
      green: {
        100: '#E7F0DC',
        200: '#CFECA8',
        300: '#B7E57A',
        400: '#A2D758',
        500: '#8DC241', //base//
        600: '#72A329',
        700: '#568019',
      },
      pink: {
        100: '#F5E4F7',
        200: '#E3A4E9',
        300: '#8B58A2',
        400: '#B245B7',
        500: '#8B298E', //base//
        600: '#691B70',
        700: '#43104A',
      },
    },
    extend: {
      borderRadius: {
        xl: '1rem',
        '2xl': '2rem',
      },
      fontFamily: {
        heading: ['Arcadia', 'Montserrat', 'Arial', 'sans-serif'],
        body: ['Source Sans Pro', 'Roboto', 'Arial', 'sans-serif'],
        navbar: ['Montserrat', 'Arial', 'sans-serif'],
        footer: ['Montserrat', 'Arial', 'sans-serif'],
        modal: ['Montserrat', 'Arial', 'sans-serif'],
      },
      height: {
        'screen-1/2': '50vh',
        'screen-1/3': '33.333333vh',
        'screen-2/3': '66.666667vh',
        'screen-1/4': '25vh',
        'screen-2/4': '50vh',
        'screen-3/4': '75vh',
        'screen-1/5': '20vh',
        'screen-2/5': '40vh',
        'screen-3/5': '60vh',
        'screen-4/5': '80vh',
        'screen-50': '50vh',
        'screen-60': '60vh',
        'screen-70': '70vh',
        'screen-80': '80vh',
        'screen-90': '90vh',
      },
      maxHeight: {
        '80': '20rem', // 320px
        '100': '25rem', // 400px
        '120': '30rem', // 480px
        '140': '35rem', // 560px
        '160': '40rem', // 640px
        '200': '50rem', // 800px
      },
      maxWidth: {
        '7xl': '80rem',
        limit: '88rem',
      },
      minWidth: {
        '1/4': '25%',
        '1/3': '33.333333%',
        '1/2': '50%',
        '3/5': '60%',
        '2/3': '66.666667%',
        '3/4': '75%',
        xs: '4rem',
        sm: '6rem',
        md: '8rem',
        lg: '10rem',
        xl: '12rem',
        '2xl': '16rem',
        '3xl': '20rem',
        '4xl': '24rem',
        '5xl': '32rem',
        '6xl': '42rem',
        '7xl': '56rem',
        '8xl': '72rem',
      },
      spacing: {
        '2px': '2px',
        '72': '18rem', // 288px
        '80': '20rem', // 320px
        '100': '25rem', // 400px
        '120': '30rem', // 480px
        '140': '35rem', // 560px
        '160': '40rem', // 640px
        '200': '50rem', // 800px
      },
    },
  },
  variants: {
    display: ['responsive', 'group-hover'],
  },
  plugins: [],
};
